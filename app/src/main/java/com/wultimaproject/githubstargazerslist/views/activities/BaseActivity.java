package com.wultimaproject.githubstargazerslist.views.activities;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import dagger.android.support.DaggerAppCompatActivity;

/**
 * Created by Antonio on 01/02/2018.
 */

abstract class BaseActivity extends DaggerAppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getContentViewResourceId() != 0) {
            setContentView(getContentViewResourceId());
        }
        bindSubviews();
    }

    protected void bindSubviews() {
    }

    abstract void showFragments(String tagFragment, Bundle arguments);

    @LayoutRes
    protected abstract int getContentViewResourceId();
}
