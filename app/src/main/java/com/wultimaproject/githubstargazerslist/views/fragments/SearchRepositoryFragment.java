package com.wultimaproject.githubstargazerslist.views.fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.wultimaproject.githubstargazerslist.R;
import com.wultimaproject.githubstargazerslist.interfaces.contracts.GenericFragmentContract;
import com.wultimaproject.githubstargazerslist.interfaces.contracts.SearchRepositoryContract;
import com.wultimaproject.githubstargazerslist.interfaces.presenters.SearchRepositoryPresenter;
import com.wultimaproject.githubstargazerslist.utils.Constants;

import javax.inject.Inject;

import static com.wultimaproject.githubstargazerslist.utils.Constants.ACTION_CLOSE_ALL;
import static com.wultimaproject.githubstargazerslist.utils.Constants.ACTION_NO_CONNECTION;
import static com.wultimaproject.githubstargazerslist.utils.Constants.TAKE_ACTION;

/**
 * Created by Antonio on 01/02/2018.
 */

public class SearchRepositoryFragment extends BaseFragment implements
                        SearchRepositoryContract.View,
                        GenericFragmentContract.Slave,
                        View.OnClickListener{

    private EditText etAuthor, etRepo;
    private Button txtOk, txtKo;
    private GenericFragmentContract.Master mMaster;
    private Context mContext;

    @Inject
    SearchRepositoryPresenter mPresenter;


    @Inject
    public SearchRepositoryFragment(){}



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mPresenter.takeView(this);
        mContext = context;

    }

    @Override
    int getFragmentView() {
        return R.layout.fragment_search_repository;
    }


    @Override
    protected void bindSubviews(@NonNull View fragmentView) {
        super.bindSubviews(fragmentView);

        TextView txtTitle = (TextView) fragmentView.findViewById(R.id.txt_title_toolbar);
        txtTitle.setText(mContext.getResources().getString(R.string.app_name));

        etAuthor = (EditText) fragmentView.findViewById(R.id.et_author);
        etRepo = (EditText) fragmentView.findViewById(R.id.et_repo);

        txtKo = (Button) fragmentView.findViewById(R.id.txt_ko);
        txtOk = (Button) fragmentView.findViewById(R.id.txt_ok);

        txtOk.setOnClickListener(this);
        txtKo.setOnClickListener(this);

    }

    @Override
    void onBackArrowPressed() {

        mMaster.onSavedData(TAKE_ACTION,ACTION_CLOSE_ALL );
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.txt_ko:
                deleteInput();
            break;

            case R.id.txt_ok:
                if (isConnectionActive()) {
                    mPresenter.checkInsertions(etAuthor.getText().toString(), etRepo.getText().toString());
                } else {
                    mMaster.onSavedData(TAKE_ACTION, ACTION_NO_CONNECTION);
                }
            break;

        }
    }

    @Override
    public void isSearchValid(Bundle search) {

        deleteInput();
        mMaster.onSavedData(Constants.TAG_SEARCH_REPOSITORY_FRAGMENT,search);

    }

    @Override
    public void onLoadedData(Object data) {

    }

    @Override
    public void settingMasterContext(GenericFragmentContract.Master contract) {
        mMaster = contract;
    }


    private void deleteInput(){

        etRepo.setText("");
        etRepo.clearFocus();
        etAuthor.setText("");
        etAuthor.clearFocus();
    }

    private boolean isConnectionActive(){
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }



}
