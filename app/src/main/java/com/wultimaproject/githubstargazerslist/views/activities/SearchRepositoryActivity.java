package com.wultimaproject.githubstargazerslist.views.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;

import com.wultimaproject.githubstargazerslist.R;
import com.wultimaproject.githubstargazerslist.interfaces.contracts.GenericFragmentContract;
import com.wultimaproject.githubstargazerslist.utils.Constants;
import com.wultimaproject.githubstargazerslist.views.fragments.SearchRepositoryFragment;
import com.wultimaproject.githubstargazerslist.views.fragments.ShowRepositoryFragment;

import javax.annotation.Nullable;

import static com.wultimaproject.githubstargazerslist.utils.Constants.ACTION_CLOSE_ALL;
import static com.wultimaproject.githubstargazerslist.utils.Constants.ACTION_NO_CONNECTION;
import static com.wultimaproject.githubstargazerslist.utils.Constants.EXTRA_NO_STARGAZERS;
import static com.wultimaproject.githubstargazerslist.utils.Constants.RESULT_NO_CONNECTION;
import static com.wultimaproject.githubstargazerslist.utils.Constants.RESULT_NO_RESULTS;
import static com.wultimaproject.githubstargazerslist.utils.Constants.RESULT_WRONG_INPUT;
import static com.wultimaproject.githubstargazerslist.utils.Constants.TAG_SEARCH_REPOSITORY_FRAGMENT;
import static com.wultimaproject.githubstargazerslist.utils.Constants.TAG_SHOW_REPOSITORY_FRAGMENT;
import static com.wultimaproject.githubstargazerslist.utils.Constants.TAKE_ACTION;

public class SearchRepositoryActivity extends BaseActivity implements GenericFragmentContract.Master {

    private LinearLayout ll;

    @Override
    protected void bindSubviews() {
        super.bindSubviews();

        ll = (LinearLayout) findViewById(R.id.ll_activity_container);
    }

    @Override
    protected void onResume() {
        super.onResume();

        showFragments(TAG_SEARCH_REPOSITORY_FRAGMENT, null);
    }

    @Override
    void showFragments(String tagFragment, Bundle arguments) {

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();


        switch (tagFragment) {

            case TAG_SEARCH_REPOSITORY_FRAGMENT:
                SearchRepositoryFragment fragmentSearch = new SearchRepositoryFragment();
                fragmentSearch.settingMasterContext(this);
                ft.replace(R.id.fl_fragment_container, fragmentSearch, TAG_SEARCH_REPOSITORY_FRAGMENT);
                ft.commit();
            break;


            case TAG_SHOW_REPOSITORY_FRAGMENT:

                final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);

                ShowRepositoryFragment fragmentShow = new ShowRepositoryFragment();
                fragmentShow.settingMasterContext(this);
                fragmentShow.setArguments(arguments);
                ft.add(R.id.fl_fragment_container, fragmentShow, TAG_SHOW_REPOSITORY_FRAGMENT);
                ft.commit();
                ft.addToBackStack(null);
            break;

        }


    }

    @Override
    protected int getContentViewResourceId() {
        return R.layout.activity_fragment_container;
    }

    @Override
    public void onSavedData(@Nullable String origin, Object data) {

        switch (origin) {

            case TAG_SEARCH_REPOSITORY_FRAGMENT:

                Bundle bundle = (Bundle) data;

                if (bundle.getBoolean(Constants.EXTRA_IS_VALID_SEARCH, false)) {
                    showFragments(TAG_SHOW_REPOSITORY_FRAGMENT, bundle);
                } else {
                    showErrorSnackbar(RESULT_WRONG_INPUT);
                }
            break;

            case TAG_SHOW_REPOSITORY_FRAGMENT:
                Bundle bundle2 = (Bundle) data;

                if (bundle2.getBoolean(EXTRA_NO_STARGAZERS)){
                    showFragments(TAG_SEARCH_REPOSITORY_FRAGMENT,null);
                    showErrorSnackbar(RESULT_NO_RESULTS);

                }
            break;


            case TAKE_ACTION:

                switch ((String)data){

                    case ACTION_CLOSE_ALL:
                        finish();
                    break;


                    case ACTION_NO_CONNECTION:
                        showErrorSnackbar(RESULT_NO_CONNECTION);
                    break;
                }

            break;
        }

    }

    private void showErrorSnackbar(String result) {
        String message = "";
        switch (result){
            case RESULT_NO_RESULTS:
                message = getResources().getString(R.string.no_results);
            break;

            case RESULT_WRONG_INPUT:
                message = getResources().getString(R.string.wrong_input);
            break;


            case RESULT_NO_CONNECTION:
                message = getResources().getString(R.string.no_connection);
            break;
        }
    Snackbar
        .make(ll,message,Snackbar.LENGTH_LONG)
        .setActionTextColor(getResources().getColor(R.color.white)).show();
    }
}
