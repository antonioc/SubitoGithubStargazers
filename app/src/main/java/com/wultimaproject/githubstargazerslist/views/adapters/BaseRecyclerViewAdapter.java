/*
 * Created by Antonio Coppola on 11/30/17 4:26 PM
 * Copyright (c) 2017. All rights reserved.
 *
 * Last modified 11/30/17 4:24 PM
 */

package com.wultimaproject.githubstargazerslist.views.adapters;

/*
 * Created by Antonio Coppola on 11/30/17 4:26 PM
 * Copyright (c) 2017. All rights reserved.
 *
 * Last modified 11/30/17 4:24 PM
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wultimaproject.githubstargazerslist.R;
import com.wultimaproject.githubstargazerslist.di.ActivityScoped;
import com.wultimaproject.githubstargazerslist.interfaces.contracts.GeneralRecAdapterContract;
import com.wultimaproject.githubstargazerslist.models.Stargazer;
import com.wultimaproject.githubstargazerslist.views.adapters.viewholders.ViewholderStargazer;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;




/**
 * Created by Antonio on 29/10/2017.
 */
@SuppressWarnings("all")
@ActivityScoped
public class BaseRecyclerViewAdapter<T> extends RecyclerView.Adapter implements GeneralRecAdapterContract.Adapter{

    private final static String TAG = BaseRecyclerViewAdapter.class.getSimpleName();

    private List<T> data;
    private int layoutViewHolder;
    private RecyclerView.ViewHolder vh;
    private Context context;
    private GeneralRecAdapterContract.ViewTopLevel mView;

    private List<Integer> highlightedIds;

    private SparseBooleanArray mSparseBooleanArray;

    @Inject
    public BaseRecyclerViewAdapter(Context context){
        this.context = context;
        mSparseBooleanArray = new SparseBooleanArray();
    }


    /**
     * Based on layoutViewHolder, choose which ViewHolder return
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(layoutViewHolder,parent,false);

        switch (layoutViewHolder)
        {
            case R.layout.cell_stargazer:
                return new ViewholderStargazer(v, context);


        }
        return null;
    }

    /**
     * Based on instanceof holder, take correct actions
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        Stargazer stargazer = (Stargazer)data.get(position);
        ((ViewholderStargazer)holder).setAvatar(stargazer.getAvatar_url(), stargazer.getLogin());

    }


    @Override
    public int getItemCount() {
        return data.size();
    }


    public void eraseData(){
        this.data = null;
    }

    public void setLayoutViewHolder(int layoutViewHolder){
        this.layoutViewHolder = layoutViewHolder;
    }

    @Override
    public void setDataToServe(Object listData) {
        this.data = (List<T>)listData;


    }

    @Override
    public void takeTopLevelView(Object view) {
        mView = (GeneralRecAdapterContract.ViewTopLevel)view;
    }



    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }



    public void clearAllSelection(){
        mSparseBooleanArray.clear();
        highlightedIds=new ArrayList<>();
        notifyDataSetChanged();

    }


    public void setItemsAlreadyOn(List<Integer> highlightItems){
        highlightedIds = highlightItems;
    }

}
