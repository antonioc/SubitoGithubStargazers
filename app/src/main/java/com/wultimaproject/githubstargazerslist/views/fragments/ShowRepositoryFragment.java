package com.wultimaproject.githubstargazerslist.views.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.wultimaproject.githubstargazerslist.R;
import com.wultimaproject.githubstargazerslist.interfaces.contracts.GeneralRecAdapterContract;
import com.wultimaproject.githubstargazerslist.interfaces.contracts.GenericFragmentContract;
import com.wultimaproject.githubstargazerslist.interfaces.contracts.ShowRepositoryContract;
import com.wultimaproject.githubstargazerslist.interfaces.presenters.ShowRepositoryPresenter;
import com.wultimaproject.githubstargazerslist.models.Stargazer;
import com.wultimaproject.githubstargazerslist.utils.Log;
import com.wultimaproject.githubstargazerslist.views.adapters.BaseRecyclerViewAdapter;
import com.wultimaproject.githubstargazerslist.views.adapters.EndlessRecyclerViewScrollListener;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import static android.view.View.GONE;
import static com.wultimaproject.githubstargazerslist.utils.Constants.EXTRA_NO_STARGAZERS;
import static com.wultimaproject.githubstargazerslist.utils.Constants.EXTRA_SEARCH_AUTHOR;
import static com.wultimaproject.githubstargazerslist.utils.Constants.EXTRA_SEARCH_REPO;
import static com.wultimaproject.githubstargazerslist.utils.Constants.TAG_SHOW_REPOSITORY_FRAGMENT;

/**
 * Created by Antonio on 02/02/2018.
 */

public class ShowRepositoryFragment extends BaseFragment implements
        GenericFragmentContract.Slave,
        ShowRepositoryContract.View,
        GeneralRecAdapterContract.ViewTopLevel,
        View.OnClickListener{

    private final static String TAG = ShowRepositoryFragment.class.getSimpleName();
    private final static int STARTING_PAGE = 1;

    private GenericFragmentContract.Master mMaster;

    private String author, repo;
    private RecyclerView rec;
    private Context mContext;
    private ProgressBar progressBar;
    private TextView txtTitle;


    private EndlessRecyclerViewScrollListener endlessScrollListener;
//    private LinearLayoutManager linearLayoutManager;
    private GridLayoutManager gridLayoutManager;

    private List<Stargazer> stargazers;

    @Inject
    BaseRecyclerViewAdapter<Stargazer> adapter;

    @Inject
    ShowRepositoryPresenter mPresenter;





    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        Bundle bundle = getArguments();
        author = bundle.getString(EXTRA_SEARCH_AUTHOR);
        repo = bundle.getString(EXTRA_SEARCH_REPO);

//        linearLayoutManager = new LinearLayoutManager(context);
        gridLayoutManager = new GridLayoutManager(context, 2);
        mPresenter.takeView(this);
        stargazers = new ArrayList<>();
        endlessScrollListener =  new EndlessRecyclerViewScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
//                mPresenter.requestProductCollections(filtersSingleShot,filtersArrayShot, page);
                Log.d(TAG, "load more! with page: "+page);
                mPresenter.getStagazers(author,repo,page);
            }
        };
    }

    @Override
    protected void bindSubviews(@NonNull View fragmentView) {
        super.bindSubviews(fragmentView);

        ImageView imgBack = (ImageView) fragmentView.findViewById(R.id.img_back_toolbar);
        imgBack.setVisibility(View.VISIBLE);
        imgBack.setOnClickListener(this);

        txtTitle = (TextView) fragmentView.findViewById(R.id.txt_title_toolbar);
        txtTitle.setText(mContext.getResources().getString(R.string.found_results));



        progressBar = (ProgressBar) fragmentView.findViewById(R.id.progress_bar);
        rec = (RecyclerView) fragmentView.findViewById(R.id.rec_stargazers);
        rec.setLayoutManager(gridLayoutManager);


    }

    @Override
    public void onResume() {
        super.onResume();
        gridLayoutManager = new GridLayoutManager(mContext, 3);
        adapter.setDataToServe(stargazers);
        adapter.setLayoutViewHolder(R.layout.cell_stargazer);
        mPresenter.getStagazers(author,repo, STARTING_PAGE);
    }

    @Override
    public void showStargazers(List<Stargazer> listStargazer) {

        progressBar.setVisibility(GONE);
        rec.setVisibility(View.VISIBLE);

        rec.addOnScrollListener(endlessScrollListener);

        if (stargazers.size() < 1){

            stargazers.addAll(listStargazer);
            adapter.notifyItemRangeInserted(0, stargazers.size() - 1);
            rec.setAdapter(adapter);


        } else {
            int curSize = adapter.getItemCount();
            stargazers.addAll(listStargazer);
            adapter.notifyItemRangeInserted(curSize, stargazers.size() - 1);
        }

    }

    @Override
    public void noStargazers() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(EXTRA_NO_STARGAZERS, true);
        mMaster.onSavedData(TAG_SHOW_REPOSITORY_FRAGMENT, bundle);
        autoFinish();
    }


    @Override
    int getFragmentView() {
        return R.layout.fragment_show_stargazers;
    }


    @Override
    public void setOnRecyclerItemClickListener(Object data) {

    }

    @Override
    void onBackArrowPressed() {
        autoFinish();

    }

    @Override
    public void onLoadedData(Object data) {

    }

    @Override
    public void settingMasterContext(GenericFragmentContract.Master contract) {
        mMaster = contract;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_back_toolbar:
                autoFinish();
            break;
        }
    }
}
