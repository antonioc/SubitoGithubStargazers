package com.wultimaproject.githubstargazerslist.views.adapters.viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.wultimaproject.githubstargazerslist.R;

/**
 * Created by Antonio on 02/02/2018.
 */

public class ViewholderStargazer extends ViewHolder {

    private ImageView img;
    private TextView txt;

    private Context context;


    public ViewholderStargazer(View itemView, Context context) {
        super(itemView);
        this.context = context;
        img = (ImageView) itemView.findViewById(R.id.img_avatar);
        txt = (TextView) itemView.findViewById(R.id.txt_avatar);
    }

    public void setAvatar(String imgAvatar, String nameAvatar){
        txt.setText(nameAvatar);

        int imageSize = img.getLayoutParams().width;

        Picasso.with(context).load(imgAvatar)
                .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .resize(imageSize/3, imageSize/3)
                .into(img);
    }
}
