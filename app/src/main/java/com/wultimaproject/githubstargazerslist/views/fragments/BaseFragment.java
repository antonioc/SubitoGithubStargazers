package com.wultimaproject.githubstargazerslist.views.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wultimaproject.githubstargazerslist.R;

/**
 * Created by Antonio on 01/02/2018.
 */

public abstract class BaseFragment extends dagger.android.support.DaggerFragment{

    private ProgressDialog progressDialog;
    private Context context;
    private View v;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(getFragmentView(), container,false);

        bindSubviews(v);

        return v;
    }

    public void startProgressDialog() {
        if (progressDialog==null || !progressDialog.isShowing()) {
            progressDialog = ProgressDialog.show(context, null, "caricamento", true, false);

        }
    }

    public void stopProgressDialog() {
        if (progressDialog!=null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    protected void setToolbarTitle(String s){

        TextView title = (TextView) v.findViewById(R.id.txt_title_toolbar);
        if (title != null){
            title.setText(s);
        }

        ImageView imgBack = (ImageView) v.findViewById(R.id.img_back_toolbar);
        if (imgBack != null){

            imgBack.setOnClickListener( v-> onBackArrowPressed());
        }
    }

    protected void bindSubviews(@NonNull View fragmentView){}


    abstract int getFragmentView();


    public void autoFinish(){
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }



    abstract void onBackArrowPressed();
}
