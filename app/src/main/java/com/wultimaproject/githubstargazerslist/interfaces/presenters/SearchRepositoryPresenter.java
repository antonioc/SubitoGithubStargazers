package com.wultimaproject.githubstargazerslist.interfaces.presenters;

import android.os.Bundle;

import com.wultimaproject.githubstargazerslist.interfaces.contracts.SearchRepositoryContract;

import javax.inject.Inject;

import static com.wultimaproject.githubstargazerslist.utils.Constants.EXTRA_IS_VALID_SEARCH;
import static com.wultimaproject.githubstargazerslist.utils.Constants.EXTRA_SEARCH_AUTHOR;
import static com.wultimaproject.githubstargazerslist.utils.Constants.EXTRA_SEARCH_REPO;

/**
 * Created by Antonio on 02/02/2018.
 */

public class SearchRepositoryPresenter implements SearchRepositoryContract.Presenter{

    SearchRepositoryContract.View mView;



    @Inject
    public SearchRepositoryPresenter(){}



    @Override
    public void takeView(SearchRepositoryContract.View view) {
        mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
    }

    @Override
    public void checkInsertions(String author, String repo) {

        Bundle bundle = new Bundle();

        if (!author.trim().equals("") && !repo.trim().equals("")){

            bundle.putBoolean(EXTRA_IS_VALID_SEARCH, true);
            bundle.putString(EXTRA_SEARCH_AUTHOR, author);
            bundle.putString(EXTRA_SEARCH_REPO, repo);
        } else {

            bundle.putBoolean(EXTRA_IS_VALID_SEARCH, false);
        }

        mView.isSearchValid(bundle);

    }


}
