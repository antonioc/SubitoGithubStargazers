/*
 * Created by Antonio Coppola on 11/30/17 4:09 PM
 * Copyright (c) 2017. All rights reserved.
 *
 * Last modified 11/29/17 3:27 PM
 */

package com.wultimaproject.githubstargazerslist.interfaces.contracts;

/**
 * Created by Antonio on 19/10/2017.
 */

public interface GeneralRecAdapterContract {

    interface Adapter<K,T>{
        void setDataToServe(T listData);
        void takeTopLevelView(K view);
    }

    interface ViewTopLevel<T>{
        void setOnRecyclerItemClickListener(T data);
    }
}
