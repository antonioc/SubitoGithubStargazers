/*
 * Created by Antonio Coppola on 11/30/17 4:09 PM
 * Copyright (c) 2017. All rights reserved.
 *
 * Last modified 11/30/17 3:50 PM
 */

package com.wultimaproject.githubstargazerslist.interfaces.contracts;

import javax.annotation.Nullable;

public interface GenericFragmentContract {

    public interface Master<T> {


        void onSavedData(@Nullable String origin, Object data);


    }

    public interface Slave<T> {


        void onLoadedData(Object data);

        void settingMasterContext(GenericFragmentContract.Master contract);
    }
}
