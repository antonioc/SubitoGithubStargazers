/*
 * Created by Antonio Coppola on 11/30/17 4:23 PM
 * Copyright (c) 2017. All rights reserved.
 *
 * Last modified 10/27/17 10:30 AM
 */

package com.wultimaproject.githubstargazerslist.interfaces.presenters;

public interface BasePresenter<T> {

    void takeView(T view);

    void dropView();
}
