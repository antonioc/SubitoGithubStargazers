package com.wultimaproject.githubstargazerslist.interfaces.contracts;

import android.os.Bundle;

import com.wultimaproject.githubstargazerslist.interfaces.presenters.BasePresenter;
import com.wultimaproject.githubstargazerslist.interfaces.presenters.BaseView;

/**
 * Created by Antonio on 02/02/2018.
 */

public interface SearchRepositoryContract {

    public interface View extends BaseView<Presenter> {
       public void isSearchValid(Bundle search);
    }

    public interface Presenter extends BasePresenter<View>{
        public void checkInsertions(String author, String repo);
    }
}
