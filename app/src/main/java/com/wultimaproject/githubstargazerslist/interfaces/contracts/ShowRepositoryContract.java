package com.wultimaproject.githubstargazerslist.interfaces.contracts;

import com.wultimaproject.githubstargazerslist.interfaces.presenters.BasePresenter;
import com.wultimaproject.githubstargazerslist.interfaces.presenters.BaseView;
import com.wultimaproject.githubstargazerslist.models.Stargazer;

import java.util.List;

/**
 * Created by Antonio on 02/02/2018.
 */

public interface ShowRepositoryContract {

    public interface View extends BaseView<Presenter>{

        public void showStargazers(List<Stargazer> listStargazer);
        public void noStargazers();

    }


    public interface Presenter extends BasePresenter<View>{
        public void getStagazers(String author, String repo, int page);
    }
}
