package com.wultimaproject.githubstargazerslist.interfaces.presenters;

import com.wultimaproject.githubstargazerslist.interfaces.contracts.ShowRepositoryContract;
import com.wultimaproject.githubstargazerslist.models.Stargazer;
import com.wultimaproject.githubstargazerslist.retrofit.NetworkInterface;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Antonio on 02/02/2018.
 */

public class ShowRepositoryPresenter implements ShowRepositoryContract.Presenter {

    private ShowRepositoryContract.View mView;


    private NetworkInterface apiInterface;


    @Inject
    public ShowRepositoryPresenter(NetworkInterface apiInterface){
        this.apiInterface = apiInterface;
    }

    public SingleObserver<List<Stargazer>> obsrStargazers = new SingleObserver<List<Stargazer>>() {
        @Override
        public void onSubscribe(Disposable d) {

        }

        @Override
        public void onSuccess(List<Stargazer> stargazers) {

            mView.showStargazers(stargazers);
        }

        @Override
        public void onError(Throwable e) {
            e.printStackTrace();
            mView.noStargazers();
        }
    };


    @Override
    public void takeView(ShowRepositoryContract.View view) {
        mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
    }


    @Override
    public void getStagazers(String author, String repo, int page) {

        Single<List<Stargazer>> obsStargazers = apiInterface.getStargazers(author,repo, page);

        obsStargazers.subscribeOn(Schedulers.newThread())
        .observeOn(Schedulers.computation())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(obsrStargazers);


    }
}
