//package com.wultimaproject.githubstargazerslist.retrofit;///*
//// * Created by Antonio Coppola on 25/10/17 10.24
//// * Copyright (c) 2017. All rights reserved.
//// *
//// * Last modified 25/10/17 10.24
//// */
////
////package com.wultimaproject.shinny.retrofit;
////
//import com.wultimaproject.githubstargazerslist.retrofit.wrappers.WrapperResponse;
//
//import java.lang.annotation.Annotation;
//import java.lang.reflect.ParameterizedType;
//import java.lang.reflect.Type;
//
//import okhttp3.ResponseBody;
//import retrofit2.Converter;
//import retrofit2.Retrofit;
//import retrofit2.converter.gson.GsonConverterFactory;
//
///**
// * Created by Antonio on 25/10/2017.
// */
//
//public class UnwrapConverterFactory extends Converter.Factory {
//
//    private GsonConverterFactory factory;
//
//    public UnwrapConverterFactory(GsonConverterFactory factory) {
//        this.factory = factory;
//    }
//
//    @Override
//    public Converter<ResponseBody, ?> responseBodyConverter(final Type type,
//                                                            Annotation[] annotations, Retrofit retrofit) {
//        Type wrappedType = new ParameterizedType() {
//            @Override
//            public Type[] getActualTypeArguments() {
//                return new Type[] {type};
//            }
//
//            @Override
//            public Type getOwnerType() {
//                return null;
//            }
//
//            @Override
//            public Type getRawType() {
//                return WrapperResponse.class;
//            }
//        };
//        Converter<ResponseBody, ?> gsonConverter = factory
//                .responseBodyConverter(wrappedType, annotations, retrofit);
//        return new WrapperResponseBodyConverter(gsonConverter);
//    }
//}