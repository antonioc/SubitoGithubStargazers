//package com.wultimaproject.githubstargazerslist.retrofit.wrappers;///*
//// * Created by Antonio Coppola on 25/10/17 12.30
//// * Copyright (c) 2017. All rights reserved.
//// *
//// * Last modified 25/10/17 12.29
//// */
//
//import com.google.gson.annotations.Expose;
//import com.google.gson.annotations.SerializedName;
//import com.wultimaproject.githubstargazerslist.models.DataResponse;
//
//public class WrapperResponse<T> {
//    @SerializedName(value="products")
//    @Expose
//DataResponse<T> data;
//
//    public DataResponse<T> getData() {
//        return data;
//    }
//
//    public void setData(DataResponse<T> data) {
//        this.data = data;
//    }
//}
