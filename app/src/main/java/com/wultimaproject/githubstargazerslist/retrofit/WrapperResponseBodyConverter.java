//package com.wultimaproject.githubstargazerslist.retrofit;///*
//// * Created by Antonio Coppola on 25/10/17 10.51
//// * Copyright (c) 2017. All rights reserved.
//// *
//// * Last modified 25/10/17 10.51
//// */
////
////package com.wultimaproject.shinny.retrofit;
////
//import com.wultimaproject.githubstargazerslist.retrofit.wrappers.WrapperResponse;
//
//import java.io.IOException;
//
//import okhttp3.ResponseBody;
//import retrofit2.Converter;
//
///**
// * Created by Antonio on 25/10/2017.
// */
//
//public class WrapperResponseBodyConverter<T>
//        implements Converter<ResponseBody, T> {
//    private Converter<ResponseBody, WrapperResponse<T>> converter;
//
//    public WrapperResponseBodyConverter(Converter<ResponseBody,
//            WrapperResponse<T>> converter) {
//        this.converter = converter;
//    }
//
//    @Override
//    public T convert(ResponseBody value) throws IOException {
//        WrapperResponse<T> response = converter.convert(value);
//            return response.getData().getData();
//    }
//}
