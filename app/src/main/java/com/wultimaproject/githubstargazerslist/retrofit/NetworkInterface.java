package com.wultimaproject.githubstargazerslist.retrofit;


import com.wultimaproject.githubstargazerslist.models.Stargazer;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface NetworkInterface {

    @GET("repos/{owner}/{repo}/stargazers")
//    Call<List<Stargazer>> getStargazers(
//            @Path("owner") String author,
//            @Path("repo") String repo);
    Single<List<Stargazer>> getStargazers(
            @Path("owner") String author,
            @Path("repo") String repo,
            @Query("page") int page);

}



