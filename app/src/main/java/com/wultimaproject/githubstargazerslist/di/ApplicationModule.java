/*
 * Created by Antonio Coppola on 10/13/17 2:42 PM
 * Copyright (c) 2017. All rights reserved.
 *
 * Last modified 10/13/17 2:42 PM
 */

package com.wultimaproject.githubstargazerslist.di;

import android.app.Application;
import android.content.Context;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class ApplicationModule {
    //expose Application as an injectable context
    @Binds
    abstract Context bindContext(Application application);

}
