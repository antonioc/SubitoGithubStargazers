/*
 * Created by Antonio Coppola on 11/23/17 3:04 PM
 * Copyright (c) 2017. All rights reserved.
 *
 * Last modified 11/23/17 3:04 PM
 */

package com.wultimaproject.githubstargazerslist.di;

import android.app.Activity;

import dagger.Binds;
import dagger.Module;


@Module
public abstract class ActivityModule {
    //expose Application as an injectable context
    @Binds
    abstract Activity bindActivityContext(Activity activity);

}
