/*
 * Created by Antonio Coppola on 10/13/17 4:21 PM
 * Copyright (c) 2017. All rights reserved.
 *
 * Last modified 10/13/17 4:21 PM
 */

package com.wultimaproject.githubstargazerslist.di;

import android.content.Context;

import com.wultimaproject.githubstargazerslist.R;
import com.wultimaproject.githubstargazerslist.retrofit.NetworkInterface;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class RetrofitModule {
    @ActivityScoped
    @Provides

    public HttpLoggingInterceptor provideHttpLogginInterceptor(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        return interceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
    }


    @ActivityScoped
    @Provides
    public OkHttpClient provideOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor){
        return new OkHttpClient.Builder()
            .addInterceptor(httpLoggingInterceptor) //comment to hide logs
            .build();
    }

    @ActivityScoped
    @Provides
    public Retrofit provideRetrofit(Context context, OkHttpClient okHttpClient){

        return new Retrofit.Builder()
                .baseUrl(context.getString(R.string.base_url))
//                .addConverterFactory(new UnwrapConverterFactory(GsonConverterFactory.create()))
                .addConverterFactory((GsonConverterFactory.create()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
    }

    @ActivityScoped
    @Provides
    public NetworkInterface provideNetworkInterface(Retrofit retrofit){
        return retrofit.create(NetworkInterface.class);
    }


}
