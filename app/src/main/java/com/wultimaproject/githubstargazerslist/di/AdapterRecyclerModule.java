/*
 * Created by Antonio Coppola on 29/10/17 18.55
 * Copyright (c) 2017. All rights reserved.
 *
 * Last modified 29/10/17 18.55
 */

package com.wultimaproject.githubstargazerslist.di;

import android.content.Context;

import com.wultimaproject.githubstargazerslist.views.adapters.BaseRecyclerViewAdapter;

import dagger.Module;
import dagger.Provides;


/**
 * Created by Antonio on 29/10/2017.
 */
@Module
public class AdapterRecyclerModule {


    @Provides
    @ActivityScoped
    public BaseRecyclerViewAdapter provideBaseRecyclerViewAdapter(Context context){
        return new BaseRecyclerViewAdapter(context);
    }

}
