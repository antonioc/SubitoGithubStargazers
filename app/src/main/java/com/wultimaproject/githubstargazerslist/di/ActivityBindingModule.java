/*
 * Created by Antonio Coppola on 10/13/17 2:40 PM
 * Copyright (c) 2017. All rights reserved.
 *
 * Last modified 10/13/17 2:40 PM
 */

package com.wultimaproject.githubstargazerslist.di;

import com.wultimaproject.githubstargazerslist.views.activities.SearchRepositoryActivity;
import com.wultimaproject.githubstargazerslist.views.fragments.SearchRepositoryFragment;
import com.wultimaproject.githubstargazerslist.views.fragments.ShowRepositoryFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {


    @ActivityScoped
    @ContributesAndroidInjector(modules = PresenterModule.class)
    abstract SearchRepositoryActivity mSearchRepositoryActivity();


    @ActivityScoped
    @ContributesAndroidInjector(modules = PresenterModule.class)
    abstract SearchRepositoryFragment mSearchRepositoryFragment();


    @ActivityScoped
    @ContributesAndroidInjector(modules = {RetrofitModule.class, AdapterRecyclerModule.class, RetrofitModule.class,PresenterModule.class})
    abstract ShowRepositoryFragment mShowRepositoryFragment();



}
