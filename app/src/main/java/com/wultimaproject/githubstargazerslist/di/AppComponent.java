/*
 * Created by Antonio Coppola on 10/13/17 2:33 PM
 * Copyright (c) 2017. All rights reserved.
 *
 * Last modified 10/13/17 2:33 PM
 */

package com.wultimaproject.githubstargazerslist.di;

import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import dagger.android.support.AndroidSupportInjectionModule;


@Singleton
@Component (modules = {ActivityBindingModule.class,
                        AndroidSupportInjectionModule.class,

                        ApplicationModule.class,
                        ActivityModule.class})



public interface AppComponent extends AndroidInjector<DaggerApplication>{


    @Override
    void inject(DaggerApplication instance);

        @Component.Builder
        interface Builder {

            @BindsInstance
            Builder application(Application application);

            AppComponent build();


        }

}

