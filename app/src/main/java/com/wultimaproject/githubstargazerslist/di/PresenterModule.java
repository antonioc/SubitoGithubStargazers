/*
 * Created by Antonio Coppola on 10/13/17 2:30 PM
 * Copyright (c) 2017. All rights reserved.
 *
 * Last modified 10/13/17 2:30 PM
 */

package com.wultimaproject.githubstargazerslist.di;


import com.wultimaproject.githubstargazerslist.interfaces.contracts.SearchRepositoryContract;
import com.wultimaproject.githubstargazerslist.interfaces.contracts.ShowRepositoryContract;
import com.wultimaproject.githubstargazerslist.interfaces.presenters.SearchRepositoryPresenter;
import com.wultimaproject.githubstargazerslist.interfaces.presenters.ShowRepositoryPresenter;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class PresenterModule {

    @ActivityScoped
    @Binds
    abstract SearchRepositoryContract.Presenter searchRepositoryPResenter(SearchRepositoryPresenter presenter);


    @ActivityScoped
    @Binds
    abstract ShowRepositoryContract.Presenter showRepositoryPResenter(ShowRepositoryPresenter presenter);


}
