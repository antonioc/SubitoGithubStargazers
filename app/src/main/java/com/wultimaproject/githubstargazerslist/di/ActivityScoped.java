/*
 * Created by Antonio Coppola on 10/13/17 12:20 PM
 * Copyright (c) 2017. All rights reserved.
 *
 * Last modified 10/13/17 12:20 PM
 */

package com.wultimaproject.githubstargazerslist.di;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

@Documented
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ActivityScoped {
}

