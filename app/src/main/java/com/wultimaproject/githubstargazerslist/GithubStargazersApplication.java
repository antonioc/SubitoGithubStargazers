package com.wultimaproject.githubstargazerslist;

import com.wultimaproject.githubstargazerslist.di.AppComponent;
import com.wultimaproject.githubstargazerslist.di.DaggerAppComponent;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

/**
 * Created by Antonio on 01/02/2018.
 */

public class GithubStargazersApplication extends DaggerApplication {


    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        AppComponent appComponent = DaggerAppComponent.builder().application(this).build();
        appComponent.inject(this);

        return appComponent;
    }

}
