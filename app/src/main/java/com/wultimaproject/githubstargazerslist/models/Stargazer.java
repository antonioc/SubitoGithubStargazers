package com.wultimaproject.githubstargazerslist.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Antonio on 02/02/2018.
 */

public class Stargazer {

    @SerializedName("avatar_url")
    @Expose
    private String avatar_url;


    @SerializedName("login")
    @Expose
    private String login;

    public Stargazer(){}

    public Stargazer(String avatarUrl, String username){
        avatar_url = avatarUrl;
        login = username;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public String toString() {
        return "Stargazer: "+login;
    }
}
