package com.wultimaproject.githubstargazerslist.models;

import com.google.gson.annotations.SerializedName;

public class DataResponse<T> {

    @SerializedName("data")
    private T data;

    public T getData() {
        return data;
    }
}