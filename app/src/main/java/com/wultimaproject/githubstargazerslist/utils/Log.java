/*
 * Created by Antonio Coppola on 20/10/17 9.48
 * Copyright (c) 2017. All rights reserved.
 *
 * Last modified 04/09/17 14.16
 */

package com.wultimaproject.githubstargazerslist.utils;


import com.wultimaproject.githubstargazerslist.BuildConfig;

/**
 * Created by antonio on 10/01/17.
 */

public class Log {
    static final boolean LOG = BuildConfig.DEBUG;


    public static void i(String tag, String string) {
        if (LOG) android.util.Log.i(tag, string);
    }
    public static void e(String tag, String string) {
        if (LOG) android.util.Log.e(tag, string);
    }
    public static void d(String tag, String string) {
        if (LOG) android.util.Log.d(tag, string);
    }
    public static void v(String tag, String string) {
        if (LOG) android.util.Log.v(tag, string);
    }
    public static void w(String tag, String string) {
        if (LOG) android.util.Log.w(tag, string);
    }
}
