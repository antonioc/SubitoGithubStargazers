package com.wultimaproject.githubstargazerslist.utils;

/**
 * Created by Antonio on 01/02/2018.
 */

public class Constants {

    public static final String ROOT = "com.wultimaproject.githubstargazerslist.root";



    public static final String TAG_SEARCH_REPOSITORY_FRAGMENT = ROOT+"_tag_search_repository_fragment";
    public static final String TAG_SHOW_REPOSITORY_FRAGMENT = ROOT+"_tag_show_repository_fragment";


    //bundles
    public static final String EXTRA_SEARCH_AUTHOR = ROOT+"_extra_search_author";
    public static final String EXTRA_SEARCH_REPO = ROOT+"_extra_search_repo";
    public static final String EXTRA_IS_VALID_SEARCH = ROOT+"_extra_is_valid_search";
    public static final String EXTRA_NO_STARGAZERS = ROOT+"_extra_no_stargazers";

    //commands
    public static final String RESULT_WRONG_INPUT = ROOT+"_result_wrong_input";
    public static final String RESULT_NO_RESULTS = ROOT+"_result_no_results";
    public static final String RESULT_NO_CONNECTION = ROOT+"_result_no_connection";
    public static final String TAKE_ACTION = ROOT+"_take_action";
    public static final String ACTION_CLOSE_ALL = ROOT+"_action_close_all";
    public static final String ACTION_NO_CONNECTION = ROOT+"_action_no_connection";
}
