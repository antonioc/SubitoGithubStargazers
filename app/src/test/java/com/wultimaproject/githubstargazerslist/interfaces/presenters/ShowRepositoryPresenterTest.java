package com.wultimaproject.githubstargazerslist.interfaces.presenters;

import com.wultimaproject.githubstargazerslist.interfaces.contracts.ShowRepositoryContract;
import com.wultimaproject.githubstargazerslist.models.Stargazer;
import com.wultimaproject.githubstargazerslist.retrofit.NetworkInterface;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.Mockito.verify;

/**
 * Created by Antonio on 03/02/2018.
 */

@RunWith(MockitoJUnitRunner.class)
public class ShowRepositoryPresenterTest {


    List<Stargazer> dummyStargazers = new ArrayList<>();


    @Mock
    ShowRepositoryContract.View mView;



    @Mock
    Single<List<Stargazer>> dummySingleStargazers;

    @Mock
    NetworkInterface networkInterface;



    @Captor
    private ArgumentCaptor<SingleObserver<List<Stargazer>>> mObserverStargazer;



    public ShowRepositoryPresenter mPresenter;

    @Before
    public void setupShowRepositoryPresenter() {

        MockitoAnnotations.initMocks(this);
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(scheduler -> Schedulers.trampoline());

        dummyStargazers.add(new Stargazer("http://www.example.com/img1", "star1"));
        dummyStargazers.add(new Stargazer("http://www.example.com/img2", "star2"));
    }

    @Test
    public void getStargazers_loadViewUI(){
        mPresenter = new ShowRepositoryPresenter(networkInterface);
        mPresenter.takeView(mView);


        Mockito.when(networkInterface.getStargazers("author0","repo",1)).thenReturn(dummySingleStargazers);
        mPresenter.getStagazers("author0","repo",1);
        mPresenter.obsrStargazers.onSuccess(dummyStargazers);

        verify(mView).showStargazers(dummyStargazers);

    }

    @Test
    public void getStargazers_NOloadViewUI(){
        mPresenter = new ShowRepositoryPresenter(networkInterface);
        mPresenter.takeView(mView);
        Mockito.when(networkInterface.getStargazers("author0","repo",1)).thenReturn(dummySingleStargazers);
        mPresenter.getStagazers("author0","repo",1);
        mPresenter.obsrStargazers.onError(new Throwable());
        verify(mView).noStargazers();

    }

}