package com.wultimaproject.githubstargazerslist.views.fragments;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;

import com.wultimaproject.githubstargazerslist.R;
import com.wultimaproject.githubstargazerslist.views.activities.SearchRepositoryActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.AllOf.allOf;

/**
 * Created by Antonio on 04/02/2018.
 */
@RunWith(AndroidJUnit4.class)
public class SearchRepositoryFragmentTest extends ActivityInstrumentationTestCase2<SearchRepositoryActivity> {


    public SearchRepositoryFragmentTest() {
        super(SearchRepositoryActivity.class);
    }

    @Rule
    public ActivityTestRule<SearchRepositoryActivity> activityTestRule =
            new ActivityTestRule<>(SearchRepositoryActivity.class);



    @Test
    public void SearchRepositoryFragmentTest_Found() throws Exception{

        onView(withId(R.id.et_author)) .perform(typeText("github"), closeSoftKeyboard());
        onView(withId(R.id.et_repo)).perform(typeText("fetch"),closeSoftKeyboard());
        onView(withId(R.id.txt_ok)).perform(click());
        onView(allOf(withId(R.id.txt_title_toolbar),withText(R.string.found_results))).check(matches(isDisplayed()));

    }



}